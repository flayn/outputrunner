﻿namespace OutputRunner
{
    using System.Windows;

    using UserControl = System.Windows.Controls.UserControl;

    /// <summary>
    /// Interaction logic for OutputRunnerView.xaml
    /// </summary>
    public partial class OutputRunnerView : UserControl
    {
        public OutputRunnerView()
        {
            InitializeComponent();
        }

        private void ToggleButton_OnChecked(object sender, RoutedEventArgs e)
        {
            ((OutputRunnerViewModel)this.DataContext).Evaluate();
        }
    }
}