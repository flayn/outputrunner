﻿namespace OutputRunner
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ArgumentsEditor.xaml
    /// </summary>
    public partial class ArgumentsEditor : Window
    {
        public string CurrentArguments { get; set; }

        public ArgumentsEditor(string currentArguments)
        {
            this.CurrentArguments = currentArguments;
            this.DataContext = this;

            this.InitializeComponent();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}