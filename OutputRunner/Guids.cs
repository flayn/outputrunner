﻿// Guids.cs
// MUST match guids.h
using System;

namespace OutputRunner
{
    internal static class GuidList
    {
        public const string guidOutputRunnerPkgString = "1cdefdc6-1f5e-4027-9bb7-773248c65070";
        public const string guidOutputRunnerCmdSetString = "c8ecc9fa-5aa1-4ddd-ae8a-ba89ca2e6e80";
        public const string guidToolWindowPersistanceString = "8f33a9db-eaba-4cc0-a744-3cdd27cc648d";

        public static readonly Guid guidOutputRunnerCmdSet = new Guid(guidOutputRunnerCmdSetString);
    };
}