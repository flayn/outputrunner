namespace OutputRunner
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    using EnvDTE;

    using EnvDTE80;

    using Microsoft.VisualStudio.Shell;

    using Process = System.Diagnostics.Process;

    /// <summary>
    ///     The output runner view model.
    /// </summary>
    public class OutputRunnerViewModel
    {
        #region Fields

        /// <summary>
        /// The _build solution.
        /// </summary>
        private readonly ICommand _buildSolution;

        /// <summary>
        /// The _clean solution.
        /// </summary>
        private readonly ICommand _cleanSolution;

        /// <summary>
        ///     The _dte 2.
        /// </summary>
        private readonly DTE2 _dte2;

        /// <summary>
        ///     The _items.
        /// </summary>
        private readonly ObservableCollection<StartProcess> _items = new ObservableCollection<StartProcess>();

        /// <summary>
        ///     The _refresh.
        /// </summary>
        private readonly ICommand _refresh;

        /// <summary>
        /// The _build events.
        /// </summary>
        private BuildEvents _buildEvents;

        /// <summary>
        /// The _solution event.
        /// </summary>
        private SolutionEvents _solutionEvent;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="OutputRunnerViewModel" /> class.
        /// </summary>
        public OutputRunnerViewModel()
        {
            this._dte2 = Package.GetGlobalService(typeof(DTE)) as DTE2;
            this._solutionEvent = this._dte2.Events.SolutionEvents; // keep local reference, otherwise events wont trigger!
            this._solutionEvent.Opened += this.Evaluate;

            this._buildEvents = this._dte2.Events.BuildEvents; // keep local reference, otherwise events wont trigger!
            this._buildEvents.OnBuildProjConfigDone += this.BuildEvents_OnBuildProjConfigDone;
            this._buildEvents.OnBuildProjConfigBegin += this.BuildEvents_OnBuildProjConfigBegin;

            this._refresh = new CommandHandler(this.Evaluate, true);
            this._buildSolution = new CommandHandler(this.OnBuildSolution, true);
            this._cleanSolution = new CommandHandler(() => this._dte2.Solution.SolutionBuild.Clean(), true);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the build solution.
        /// </summary>
        public ICommand BuildSolution
        {
            get
            {
                return this._buildSolution;
            }
        }

        /// <summary>
        /// Gets the clean solution.
        /// </summary>
        public ICommand CleanSolution
        {
            get
            {
                return this._cleanSolution;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether exe only.
        /// </summary>
        public bool ExeOnly { get; set; }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public ObservableCollection<StartProcess> Items
        {
            get
            {
                return this._items;
            }
        }

        /// <summary>
        /// Gets the refresh.
        /// </summary>
        public ICommand Refresh
        {
            get
            {
                return this._refresh;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The evaluate.
        /// </summary>
        public void Evaluate()
        {
            var sb = (SolutionBuild2)this._dte2.Solution.SolutionBuild;
            var startupProjects = new List<string>(0);
            if (sb.StartupProjects != null)
            {
                startupProjects = ((Array)sb.StartupProjects).Cast<string>().ToList();
            }
            else
            {
                var singleStartupProject = (string)this._dte2.Solution.Properties.Item("StartupProject").Value;
                startupProjects.Add(singleStartupProject);
            }

            var currentSolutionProjects = new List<string>();

            foreach (Project project in this.GetSolutionProjects())
            {
                string displayName = string.Empty;
                try
                {
                    displayName = project.Name;
                    string uniqueProjectName = project.UniqueName;
                    string fullPath = project.Properties.Item("FullPath").Value.ToString();
                    string outputPath = project.ConfigurationManager.ActiveConfiguration.Properties.Item("OutputPath").Value.ToString();
                    string outputDir = Path.Combine(fullPath, outputPath);
                    string outputFileName = project.Properties.Item("OutputFileName").Value.ToString();
                    string assemblyPath = Path.Combine(outputDir, outputFileName);

                    ImageSource imageSource = null;

                    bool include = !this.ExeOnly || assemblyPath.EndsWith("exe");

                    if (include)
                    {
                        currentSolutionProjects.Add(project.UniqueName);

                        string path = null;
                        string buildTime = string.Empty;

                        if (File.Exists(assemblyPath))
                        {
                            path = assemblyPath;
                            DateTime time = new FileInfo(path).LastWriteTime;

                            buildTime = time.ToShortTimeString() + " " + time.ToShortDateString();

                            using (Icon icon = Icon.ExtractAssociatedIcon(path))
                            {
                                if (icon != null)
                                {
                                    using (Bitmap bitmap = icon.ToBitmap())
                                    {
                                        imageSource = this.BitmapToImageSource(bitmap);
                                    }
                                }
                            }
                        }

                        Configuration configuration = project.ConfigurationManager.ActiveConfiguration;
                        var arguments = (string)configuration.Properties.Item("StartArguments").Value;

                        bool alreadyExists;

                        StartProcess item = this.UpdateOrCreateItem(
                            uniqueProjectName, 
                            displayName, 
                            path, 
                            arguments, 
                            buildTime, 
                            imageSource, 
                            startupProjects.Contains(uniqueProjectName), 
                            out alreadyExists);

                        if (!alreadyExists)
                        {
                            this._items.Add(item);
                        }
                    }
                }
                catch (Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine("Error loading project info for " + displayName + ": " + exception.Message);
                }
            }

            // cleanup removed projects
            StartProcess[] removedProjects = this._items.Where(i => !currentSolutionProjects.Contains(i.UniqueProjectName)).ToArray();

            foreach (StartProcess startProcess in removedProjects)
            {
                startProcess.RunExe -= this.StartProcess;
                startProcess.EditArguments -= this.EditArguments;
                startProcess.StartDebug -= this.Debug;
                startProcess.StartBuild -= this.Build;

                StartProcess item = this._items.FirstOrDefault(i => i.UniqueProjectName == startProcess.UniqueProjectName);

                if (item != null)
                {
                    this._items.Remove(item);
                }
            }
        }

        /// <summary>
        /// The get solution projects.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<Project> GetSolutionProjects()
        {
            Projects projects = this._dte2.Solution.Projects;
            var list = new List<Project>();
            IEnumerator item = projects.GetEnumerator();
            while (item.MoveNext())
            {
                var project = item.Current as Project;
                if (project == null)
                {
                    continue;
                }

                if (project.Kind == ProjectKinds.vsProjectKindSolutionFolder)
                {
                    list.AddRange(GetSolutionFolderProjects(project));
                }
                else
                {
                    list.Add(project);
                }
            }

            return list;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The get solution folder projects.
        /// </summary>
        /// <param name="solutionFolder">
        /// The solution folder.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        private static IEnumerable<Project> GetSolutionFolderProjects(Project solutionFolder)
        {
            var list = new List<Project>();
            for (int i = 1; i <= solutionFolder.ProjectItems.Count; i++)
            {
                Project subProject = solutionFolder.ProjectItems.Item(i).SubProject;
                if (subProject == null)
                {
                    continue;
                }

                // If this is another solution folder, do a recursive call, otherwise add
                if (subProject.Kind == ProjectKinds.vsProjectKindSolutionFolder)
                {
                    list.AddRange(GetSolutionFolderProjects(subProject));
                }
                else
                {
                    list.Add(subProject);
                }
            }

            return list;
        }

        /// <summary>
        /// The bitmap to image source.
        /// </summary>
        /// <param name="bitmap">
        /// The bitmap.
        /// </param>
        /// <returns>
        /// The <see cref="BitmapImage"/>.
        /// </returns>
        private BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (var memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return bitmapImage;
            }
        }

        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Build(object sender, EventArgs e)
        {
            var process = (StartProcess)sender;

            this.Save(process);

            try
            {
                this._dte2.Solution.SolutionBuild.BuildProject(this._dte2.Solution.SolutionBuild.ActiveConfiguration.Name, process.UniqueProjectName);
            }
            catch (Exception exception)
            {
                this.UpdateProjectBuildState(process, exception.Message);
            }
        }

        /// <summary>
        /// The build and run.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        private void BuildAndRun(object state)
        {
            var startProcess = (StartProcess)state;

            try
            {
                this.Save(startProcess);

                this._dte2.Solution.SolutionBuild.BuildProject(
                    this._dte2.Solution.SolutionBuild.ActiveConfiguration.Name, 
                    startProcess.UniqueProjectName, 
                    true);

                this.UpdateProjectBuildState(startProcess, "Starting...");

                var processStartInfo = new ProcessStartInfo(startProcess.Path);
                processStartInfo.Arguments = startProcess.Args;
                processStartInfo.WorkingDirectory = Path.GetDirectoryName(startProcess.Path);

                Process process = Process.Start(processStartInfo);
                Processes processes = this._dte2.Debugger.LocalProcesses;
                foreach (EnvDTE.Process proc in processes)
                {
                    if (proc.ProcessID == process.Id)
                    {
                        System.Diagnostics.Debug.Write("Attaching to: " + proc.Name + " - " + proc.ProcessID);
                        proc.Attach();

                        startProcess.Process = process;
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.Write("Error debugging: " + e.Message);

                this.UpdateProjectBuildState(startProcess, e.Message);
            }
        }

        /// <summary>
        /// The build events_ on build proj config begin.
        /// </summary>
        /// <param name="project">
        /// The project.
        /// </param>
        /// <param name="projectconfig">
        /// The projectconfig.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="solutionconfig">
        /// The solutionconfig.
        /// </param>
        private void BuildEvents_OnBuildProjConfigBegin(string project, string projectconfig, string platform, string solutionconfig)
        {
            bool found;

            StartProcess startProcess = this.TryGetStartProcessByNameOrUniqueName(project, out found);

            if (!found)
            {
                return;
            }

            this.UpdateProjectBuildState(startProcess, "Building");
        }

        /// <summary>
        /// The build events_ on build proj config done.
        /// </summary>
        /// <param name="project">
        /// The project.
        /// </param>
        /// <param name="projectconfig">
        /// The projectconfig.
        /// </param>
        /// <param name="platform">
        /// The platform.
        /// </param>
        /// <param name="solutionconfig">
        /// The solutionconfig.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        private void BuildEvents_OnBuildProjConfigDone(string project, string projectconfig, string platform, string solutionconfig, bool success)
        {
            this.UpdateProjectBuildState(project, success);
            this.Evaluate();
        }

        /// <summary>
        /// The debug.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Debug(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(this.BuildAndRun, sender);
        }

        /// <summary>
        /// The edit arguments.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void EditArguments(object sender, EventArgs e)
        {
            var sp = (StartProcess)sender;
            var editor = new ArgumentsEditor(sp.Args);
            editor.ShowDialog();

            foreach (Project project in this.GetSolutionProjects())
            {
                if (sp.UniqueProjectName == project.UniqueName)
                {
                    Configuration configuration = project.ConfigurationManager.ActiveConfiguration;
                    configuration.Properties.Item("StartArguments").Value = editor.CurrentArguments;

                    break;
                }
            }

            this.Evaluate();
        }

        /// <summary>
        /// The on build solution.
        /// </summary>
        private void OnBuildSolution()
        {
            this._dte2.ExecuteCommand("File.SaveAll");

            this._dte2.Solution.SolutionBuild.Build();
        }

        /// <summary>
        /// The save.
        /// </summary>
        /// <param name="startProcess">
        /// The start process.
        /// </param>
        private void Save(StartProcess startProcess)
        {
            bool success;
            Project project = this.TryGetProjectByNameOrUniqueName(startProcess.UniqueProjectName, out success);

            if (!success)
            {
                return;
            }

            this.UpdateProjectBuildState(startProcess, "Saving...");

            this._dte2.ExecuteCommand("File.SaveAll");

            this.UpdateProjectBuildState(startProcess, string.Empty);
        }

        /// <summary>
        /// The start process.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void StartProcess(object sender, EventArgs e)
        {
            var startProcess = (StartProcess)sender;

            var processStartInfo = new ProcessStartInfo(startProcess.Path);
            processStartInfo.Arguments = startProcess.Args;
            processStartInfo.WorkingDirectory = Path.GetDirectoryName(startProcess.Path);

            using (Process.Start(processStartInfo));
        }

        /// <summary>
        /// The toggle startup.
        /// </summary>
        /// <param name="projectName">
        /// The project name.
        /// </param>
        /// <param name="isChecked">
        /// The is checked.
        /// </param>
        private void ToggleStartup(string projectName, bool isChecked)
        {
            var sb = (SolutionBuild2)this._dte2.Solution.SolutionBuild;
            var startupProjects = new List<string>(0);

            if (sb.StartupProjects != null)
            {
                startupProjects = ((Array)sb.StartupProjects).Cast<string>().ToList();
            }

            bool changed = false;

            if (!isChecked)
            {
                changed = startupProjects.Remove(projectName);
            }
            else
            {
                if (!startupProjects.Contains(projectName))
                {
                    startupProjects.Add(projectName);
                    changed = true;
                }
            }

            if (changed)
            {
                if (startupProjects.Count == 1)
                {
                    // find the project name, the unique name cannot be set as single startup project
                    foreach (Project project in this.GetSolutionProjects())
                    {
                        if (project.UniqueName == startupProjects.First())
                        {
                            this._dte2.Solution.Properties.Item("StartupProject").Value = project.Name;
                            break;
                        }
                    }
                }
                else
                {
                    this._dte2.Solution.SolutionBuild.StartupProjects = startupProjects.Cast<object>().ToArray();
                }
            }
        }

        /// <summary>
        /// The try get project by name or unique name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        /// <returns>
        /// The <see cref="Project"/>.
        /// </returns>
        private Project TryGetProjectByNameOrUniqueName(string name, out bool success)
        {
            foreach (Project project in this.GetSolutionProjects())
            {
                if (name == project.Name || name == project.UniqueName)
                {
                    success = true;
                    return project;
                }
            }

            success = false;
            return null;
        }

        /// <summary>
        /// The try get start process by name or unique name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        /// <returns>
        /// The <see cref="StartProcess"/>.
        /// </returns>
        private StartProcess TryGetStartProcessByNameOrUniqueName(string name, out bool success)
        {
            StartProcess process = this._items.FirstOrDefault(i => i.Name == name || i.UniqueProjectName == name);

            success = process != null;

            return process;
        }

        /// <summary>
        /// The update or create item.
        /// </summary>
        /// <param name="uniqueProjectName">
        /// The unique project name.
        /// </param>
        /// <param name="displayName">
        /// The display name.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        /// <param name="buildTime">
        /// The build time.
        /// </param>
        /// <param name="imageSource">
        /// The image source.
        /// </param>
        /// <param name="isStartupProject">
        /// The is startup project.
        /// </param>
        /// <param name="alreadyExists">
        /// The already exists.
        /// </param>
        /// <returns>
        /// The <see cref="StartProcess"/>.
        /// </returns>
        private StartProcess UpdateOrCreateItem(
            string uniqueProjectName, 
            string displayName, 
            string path, 
            string arguments, 
            string buildTime, 
            ImageSource imageSource, 
            bool isStartupProject, 
            out bool alreadyExists)
        {
            StartProcess existing = this._items.FirstOrDefault(i => i.UniqueProjectName == uniqueProjectName);

            if (existing == null)
            {
                var newItem = new StartProcess(
                    uniqueProjectName, 
                    displayName, 
                    path, 
                    arguments, 
                    buildTime, 
                    imageSource, 
                    isStartupProject, 
                    this.ToggleStartup);

                newItem.RunExe += this.StartProcess;
                newItem.EditArguments += this.EditArguments;
                newItem.StartDebug += this.Debug;
                newItem.StartBuild += this.Build;

                alreadyExists = false;
                return newItem;
            }

            existing.Args = arguments;
            existing.BuildTime = buildTime;
            existing.Icon = imageSource;
            existing.IsStartupProject = isStartupProject;
            existing.Path = path;

            alreadyExists = true;
            return existing;
        }

        /// <summary>
        /// The update project build state.
        /// </summary>
        /// <param name="projectName">
        /// The project name.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        private void UpdateProjectBuildState(string projectName, bool success)
        {
            bool found;

            StartProcess startProcess = this.TryGetStartProcessByNameOrUniqueName(projectName, out found);

            if (!found)
            {
                return;
            }

            this.UpdateProjectBuildState(startProcess, success ? string.Empty : "Failed");
        }

        /// <summary>
        /// The update project build state.
        /// </summary>
        /// <param name="startProcess">
        /// The start process.
        /// </param>
        /// <param name="state">
        /// The state.
        /// </param>
        private void UpdateProjectBuildState(StartProcess startProcess, string state)
        {
            startProcess.CurrentAction = state;
        }

        #endregion
    }
}