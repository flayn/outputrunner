﻿namespace OutputRunner
{
    using OutputRunner.Annotations;
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Windows.Input;
    using System.Windows.Media;

    public sealed class StartProcess : INotifyPropertyChanged
    {
        private readonly ICommand _build;
        private readonly ICommand _debug;
        private readonly ICommand _edit;
        private readonly string _name;
        private readonly ICommand _openOutputFolder;
        private readonly ICommand _run;
        private readonly Action<string, bool> _toggleStartup;
        private readonly string _uniqueProjectName;

        private string _args;
        private string _buildTime;
        private string _currentAction = string.Empty;
        private ImageSource _icon;
        private bool _isStartupProject;
        private string _path;
        private Process _process;

        public StartProcess(string uniqueProjectName, string name, string path, string args, string buildTime, ImageSource icon, bool isStartupProject, Action<string, bool> toggleStartup)
        {
            this._run = new CommandHandler(this.OnRun, !string.IsNullOrEmpty(path));
            this._edit = new CommandHandler(this.OnEditArguments, true);
            this._debug = new CommandHandler(this.OnDebug, true);
            this._build = new CommandHandler(this.OnBuild, true);
            this._openOutputFolder = new CommandHandler(this.OnOpenOutputFolder, true);

            this._uniqueProjectName = uniqueProjectName;
            this._name = name;
            this._buildTime = buildTime;
            this._icon = icon;
            this._toggleStartup = toggleStartup;
            this.IsStartupProject = isStartupProject;
            this._path = path;
            this._args = args;
        }

        public event EventHandler EditArguments;

        public event PropertyChangedEventHandler PropertyChanged;

        public event EventHandler RunExe;

        public event EventHandler StartBuild;

        public event EventHandler StartDebug;

        public string Args
        {
            get
            {
                return this._args;
            }
            set
            {
                this._args = value;
                this.OnPropertyChanged();
            }
        }

        public ICommand Build
        {
            get
            {
                return this._build;
            }
        }

        public string BuildTime
        {
            get
            {
                return this._buildTime;
            }
            set
            {
                if (this._buildTime != value)
                {
                    this._buildTime = value;
                    this.OnPropertyChanged();
                }
            }
        }

        public string CurrentAction
        {
            get
            {
                return this._currentAction;
            }
            set
            {
                this._currentAction = value;

                this.OnPropertyChanged("CurrentAction");
            }
        }

        public ICommand Debug
        {
            get
            {
                return this._debug;
            }
        }

        public ICommand Edit
        {
            get
            {
                return this._edit;
            }
        }

        public ImageSource Icon
        {
            get
            {
                return this._icon;
            }
            set
            {
                this._icon = value;
                this.OnPropertyChanged();
            }
        }

        public bool IsStartupProject
        {
            get
            {
                return this._isStartupProject;
            }
            set
            {
                this._toggleStartup(this._uniqueProjectName, value);
                this._isStartupProject = value;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
        }

        public ICommand OpenOutputFolder
        {
            get
            {
                return this._openOutputFolder;
            }
        }

        public string Path
        {
            get
            {
                return this._path;
            }
            set
            {
                this._path = value;
                this.OnPropertyChanged("Path");
            }
        }

        public Process Process
        {
            get
            {
                return this._process;
            }
            set
            {
                if (this._process != null)
                {
                    this._process.Exited -= this.OnProcessExited;
                }

                this._process = value;
                var process = this._process;
                if (process != null)
                {
                    process.EnableRaisingEvents = true;
                    process.Exited += this.OnProcessExited;
                    this.CurrentAction = string.Format("Attached (" + process.Id + ")");
                }
                else
                {
                    this.CurrentAction = string.Empty;
                }
            }
        }

        public ICommand Run
        {
            get
            {
                return this._run;
            }
        }

        public string UniqueProjectName
        {
            get
            {
                return this._uniqueProjectName;
            }
        }

        public void OnProcessExited(object sender, EventArgs e)
        {
            this.CurrentAction = string.Empty;
        }

        private void OnBuild()
        {
            EventHandler handler = this.StartBuild;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnDebug()
        {
            EventHandler handler = this.StartDebug;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnEditArguments()
        {
            EventHandler handler = this.EditArguments;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        private void OnOpenOutputFolder()
        {
            string args = string.Format("/select, \"{0}\"", this.Path);
            var processStartInfo = new ProcessStartInfo("Explorer.exe", args);

            using (Process.Start(processStartInfo))
            {
            }
        }

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void OnRun()
        {
            if (this.RunExe != null && !string.IsNullOrEmpty(this.Path))
            {
                this.RunExe(this, new EventArgs());
            }
        }
    }
}